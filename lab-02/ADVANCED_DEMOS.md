# Using the otel-cli Demo Scripts (Pt 2)

So we've got two demo scripts left to look at, let's see if they have anything interesting going on.

#### 10-span-background-simple.sh

```shell
#!/bin/bash
# an otel-cli demo of span background

../otel-cli span background \
    --service $0 \
    --name "executing $0" \
    --timeout 2 &
#               ^ run otel-cli in the background
sleep 1

# that's it, that's the demo
# when this script exits, otel-cli will exit too so total runtime will
# be a bit over 1 second
```

*Explanation:*
- `--timeout 2` This argument tells otel-cli 2 waits this many seconds before giving up on the data returning and sending the data incomplete
- `&` This is a shell convention to "background" the current shell and create a new one. This means we don't need to preface all our bash scripts with `./otel-cli exec` 


So we create and name a span, and the sleep for 1 second before sending it to the collector.

It's only 13 lines, let's take a look at it in action!

```shell
10-span-background-simple.sh
```
```
# trace id: 6aa8cce019a24fc597c43c3452099e8d
#  span id: 1d480a22443cd789
TRACEPARENT=00-6aa8cce019a24fc597c43c3452099e8d-1d480a22443cd789-01
```

#### 15span-background-layered.sh

Awesome! We're on to the final one, I bet it won't be too complicated. Let's see...


```shell
#!/bin/bash
# an otel-cli demo of span background
#
# This demo shows span background functionality with events added to the span
# while it's running in the background, then a child span is created and
# the background span is ended gracefully.

set -e
set -x

carrier=$(mktemp)    # traceparent propagation via tempfile
sockdir=$(mktemp -d) # a unix socket will be created here

# start the span background server, set up trace propagation, and
# time out after 10 seconds (which shouldn't be reached)
../otel-cli span background \
    --tp-carrier $carrier \
    --sockdir $sockdir \
    --tp-print \
    --service $0 \
    --name "$0 script execution" \
    --timeout 10 &

data1=$(uuidgen)

# add an event to the span running in the background, with an attribute
# set to the uuid we just generated
../otel-cli span event \
    --name "did a thing" \
    --sockdir $sockdir \
    --attrs "data1=$data1"

# waste some time
sleep 1

# add an event that says we wasted some time
../otel-cli span event --name "slept 1 second" --sockdir $sockdir

# run a shorter sleep inside a child span, also note that this is using
# --tp-required so this will fail loudly if there is no traceparent
# available
../otel-cli exec \
    --service $0 \
    --name "sleep 0.2" \
    --tp-required \
    --tp-carrier $carrier \
    --tp-print \
    sleep 0.2

# finally, tell the background server we're all done and it can exit
../otel-cli span end --sockdir $sockdir
```

Oh, OK. There's a few new things in this one.

*Explanation:*
- `set -ex` `e` exits the script if an error is encountered. `x` write a trace of each command to standard error
- `sockdir=$(mktemp -d)` creates a new directory to use as a [unix socket](https://softwareengineering.stackexchange.com/questions/135968/simple-explanation-of-the-unix-sockets)
- `../otel-cli span background` creates a span and backgrounds the process, just like the previous script
- `../otel-cli span event` creates events as part of that span with specified baggage/attributes
- `../otel-cli exec sleep 0.2` kicks off another event inside the child span event we created above
- `../otel-cli span end --sockdir $sockdir` closes the span.

So a lot going on there, does anyone have guesses what this may look like in Jaeger?

Let's take a look!

```shell
15span-background-layered.sh
```
```
++ mktemp
+ carrier=/tmp/tmp.ZeCb4039b1
++ mktemp -d
+ sockdir=/tmp/tmp.r2LSsMM8EH
++ uuidgen
+ ../otel-cli span background --tp-carrier /tmp/tmp.ZeCb4039b1 --sockdir /tmp/tmp.r2LSsMM8EH --tp-print --service ./15span-background-layered.sh --name './15span-background-layered.sh script execution' --timeout 10
+ data1=da053f0a-1e36-44c7-b3b1-29177f5ebc35
+ ../otel-cli span event --name 'did a thing' --sockdir /tmp/tmp.r2LSsMM8EH --attrs data1=da053f0a-1e36-44c7-b3b1-29177f5ebc35
# trace id: 5e73bca02bd6e4ae2887386cf4d0fee3
#  span id: 04aaa4e14801c8ad
TRACEPARENT=00-5e73bca02bd6e4ae2887386cf4d0fee3-04aaa4e14801c8ad-01
+ sleep 1
+ ../otel-cli span event --name 'slept 1 second' --sockdir /tmp/tmp.r2LSsMM8EH
+ ../otel-cli exec --service ./15span-background-layered.sh --name 'sleep 0.2' --tp-required --tp-carrier /tmp/tmp.ZeCb4039b1 --tp-print sleep 0.2
# trace id: 5e73bca02bd6e4ae2887386cf4d0fee3
#  span id: 1278a2370ade0d5d
TRACEPARENT=00-5e73bca02bd6e4ae2887386cf4d0fee3-1278a2370ade0d5d-01
```
